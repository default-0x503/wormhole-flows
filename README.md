## Inspiration
Data turned into visual stories and insights can lead to enormous impacts. I am inspired to build a visualizer tool to help users make sense of Wormhole data easily so that they can find answers and insights within the state of Wormhole and build wonderful use-cases on top of Wormhole.

## What it does
The visualizer tool has 3 simple but powerful capabilities:
1. **Flows** - visualize overall flows inside Wormhole for different durations (all time, 30 days, etc) using circular flows chart (chord diagram) and more simplified linear flows chart (sankey diagram).
2. **Coins Compare** - compare and analyze side-by-side of flows of various coins (i.e: how are leading coins (SOL, ETH, LUNA) of various chains get transferred in comparison to one another?)
3. **Coins Treemap** - dig deeper into transfer patterns of various coins within various network in relation to volumes of one another. (i.e: which chains are transferring which coins to Solana? which coins are transferred to where from Solana?)

## How we built it
Simple web frontend using a charting library and JSON data extracted from Wormhole's bigtable.

## Challenges we ran into
As Wormhole data APIs are not public yet, I needed to figure out how to index and grab data from various contract addresses. After prototyping in a few ways, I decided to peek into the bigtable dataset and build on top of it.

## Accomplishments that we're proud of
Ready-to-use visualizer tool https://wormhole-flows.netlify.app/ built within short timeframe

## What we learned
1. immense volume of value flows inside Wormhole
2. interesting relations among various chains (a peek into the future of Multi-chains)
3. how data can empower decisions and actions

## What's next for Flows of Wormhole
1. let users use and improve with feedbacks
2. build an advanced data visualizer tool for Wormhole
3. build an easy to use data query APIs for Wormhole